import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/oncostation',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/',
    name: 'SignIn',
    component: () => import('../views/SignIn.vue')
  },
  {
    path: '/signup',
    name: 'SignUp',
    component: () => import('../views/SignUp.vue')
  },
  {
    path: '/appointments',
    name: 'Appointment',
    component: () => import('../views/Appointments.vue')
  },
  {
    path: '/videocall',
    name: 'VideoCall',
    component: () => import('../views/VideoCall.vue')
  },
  {
    path: '/admin/oncostation',
    name: 'AdminStation',
    component: () => import('../views/AdminStation.vue')
  },
  {
    path: '/admin/appointment',
    name: 'AdminAppointment',
    component: () => import('../views/AdminAppointment.vue')
  },
  {
    path: '/admin/patients',
    name: 'AdminPatient',
    component: () => import('../views/AdminPatient.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
